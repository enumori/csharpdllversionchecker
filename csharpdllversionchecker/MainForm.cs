﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace csharpdllversionchecker
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void _FileOpen_Click(object sender, EventArgs e)
        {
            if (_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                _FileNameTextBox.Text = _OpenFileDialog.FileName;
                _ShowVersion.Text = "ファイル名 : \r\n" + _OpenFileDialog.FileName + "\r\n\r\n" +
                                    "CLR のバージョン : \r\n" + Assembly.LoadFile(_OpenFileDialog.FileName).ImageRuntimeVersion;
            }
        }
    }
}
