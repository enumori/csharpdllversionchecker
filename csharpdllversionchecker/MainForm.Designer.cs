﻿namespace csharpdllversionchecker
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this._FileNameTextBox = new System.Windows.Forms.TextBox();
            this._FileOpen = new System.Windows.Forms.Button();
            this._ShowVersion = new System.Windows.Forms.TextBox();
            this._OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // _FileNameTextBox
            // 
            this._FileNameTextBox.Location = new System.Drawing.Point(13, 13);
            this._FileNameTextBox.Name = "_FileNameTextBox";
            this._FileNameTextBox.Size = new System.Drawing.Size(338, 19);
            this._FileNameTextBox.TabIndex = 0;
            // 
            // _FileOpen
            // 
            this._FileOpen.Location = new System.Drawing.Point(357, 11);
            this._FileOpen.Name = "_FileOpen";
            this._FileOpen.Size = new System.Drawing.Size(75, 23);
            this._FileOpen.TabIndex = 1;
            this._FileOpen.Text = "開く";
            this._FileOpen.UseVisualStyleBackColor = true;
            this._FileOpen.Click += new System.EventHandler(this._FileOpen_Click);
            // 
            // _ShowVersion
            // 
            this._ShowVersion.Location = new System.Drawing.Point(13, 39);
            this._ShowVersion.Multiline = true;
            this._ShowVersion.Name = "_ShowVersion";
            this._ShowVersion.Size = new System.Drawing.Size(419, 402);
            this._ShowVersion.TabIndex = 2;
            // 
            // _OpenFileDialog
            // 
            this._OpenFileDialog.Filter = "dll files (*.dll)|*.dll";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 453);
            this.Controls.Add(this._ShowVersion);
            this.Controls.Add(this._FileOpen);
            this.Controls.Add(this._FileNameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "C#のDLLバージョンを☑";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _FileNameTextBox;
        private System.Windows.Forms.Button _FileOpen;
        private System.Windows.Forms.TextBox _ShowVersion;
        private System.Windows.Forms.OpenFileDialog _OpenFileDialog;
    }
}

